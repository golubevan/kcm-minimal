/*
    SPDX-FileCopyrightText: 2023 Anton Golubev <golubevan@altlinux.org>

    SPDX-License-Identifier: GPL-3.0-or-later
*/

#ifndef KCM_MINIMAL_H_
#define KCM_MINIMAL_H_

#include <KQuickAddons/ManagedConfigModule>

class KcmMinimal: public KQuickAddons::ManagedConfigModule
{
    Q_OBJECT
public:
    explicit KcmMinimal(QObject *parent, const KPluginMetaData &data, const QVariantList &args);

public Q_SLOTS:
    void load() override {};
    void save() override {};
    void defaults() override {};
};


#endif // KCM_MINIMAL_H_
