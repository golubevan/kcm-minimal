/*
    SPDX-FileCopyrightText: 2023 Anton Golubev <golubevan@altlinux.org>

    SPDX-License-Identifier: GPL-3.0-or-later
*/

import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    id: root
    Label {
        x: 10
        y: 10
        text: "HELLO!"
    }
}
