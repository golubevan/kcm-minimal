
/*
    SPDX-FileCopyrightText: 2023 Anton Golubev <golubevan@altlinux.org>

    SPDX-License-Identifier: GPL-3.0-or-later
*/

#include "kcm_minimal.h"

#include <KPluginFactory>

K_PLUGIN_CLASS_WITH_JSON(KcmMinimal, "kcm_minimal.json")

KcmMinimal::KcmMinimal(QObject *parent, const KPluginMetaData &data, const QVariantList &args)
    : KQuickAddons::ManagedConfigModule(parent, data, args)
{
}

#include "kcm_minimal.moc"
